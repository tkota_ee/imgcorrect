# -*- coding: utf-8 -*-

import os
import time
import traceback
import flickrapi
from urllib.request import urlretrieve
import sys
from retry import retry
import setting

flickr_api_key = setting.API
secret_key = setting.SK

keyword = sys.argv[1]

@retry()
def get_photos(url, filepath):
    urlretrieve(url, filepath)
    time.sleep(1)

if __name__ == '__main__':

    flicker = flickrapi.FlickrAPI(flickr_api_key, secret_key, format='parsed-json')
    response = flicker.photos.search(
        text=keyword,
        per_page=2000,
        media='photos',
        sort='relevance',
        safe_search=1,
        extras='url_z,license'
    )
    photos = response['photos']

    try:
        if not os.path.exists('./image-data/' + keyword):
            os.mkdir('./image-data/' + keyword)

        for photo in photos['photo']:
            try:
                url_z = photo['url_z']
                filepath = './image-data/' + keyword + '/' + photo['id'] + '.jpg'
                get_photos(url_z, filepath)
                print(filepath, " Done!!")
            except KeyError:
                print(filepath, " Fail!!")

    except KeyError:
        traceback.print_exc()
