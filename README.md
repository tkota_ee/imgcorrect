# README #

Correcting Image Script for Machine Lerning.


### How to use? ###

- Python version:
	* Python 3.6.2 :: Anaconda custom (64-bit)
- Library:
	* os
	* time
	* traceback
	* flickrapi
	* urllib.request
	* sys
	* retry 
	* sys
	* pprint
- Command:
	* python img_get.py keyword
- Sample image:

	* ![sample1](https://i.imgur.com/urQT540.jpg)
